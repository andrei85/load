from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required


urlpatterns = [
    url(r'^$', login_required(
        TemplateView.as_view(template_name='analytics/00_dashboard.html')), name='analytics'),
    # url(r'^programs/$', TemplateView.as_view(template_name='programs.html'), name='programs'),
]
