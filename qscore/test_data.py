from program_manager.models import BatchProgram
from qscore.models import QualityScore
from accounts.models import CustomUser

def create_test_data():
    user = CustomUser.objects.get(id=1)
    program = BatchProgram(name='Dreamforce 2015', user=user)
    program.save()

    qs_1 = QualityScore(score=45, program=program)
    qs_1.save()
    qs_2 = QualityScore(score=69, program=program)
    qs_2.save()
    qs_3 = QualityScore(score=75, program=program)
    qs_3.save()
    qs_4 = QualityScore(score=89, program=program)
    qs_4.save()
    qs_5 = QualityScore(score=97, program=program)
    qs_5.save()
    qs_6 = QualityScore(score=99, program=program)
    qs_6.save()
