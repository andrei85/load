# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('qscore', '0002_remove_qualityscore_progress'),
    ]

    operations = [
        migrations.AddField(
            model_name='qualityscore',
            name='progress',
            field=models.CharField(max_length=255, blank=True),
        ),
    ]
