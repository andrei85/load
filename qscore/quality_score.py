from qscore.prepare import *


class QualityScoreCalculator(object):

    POINT_RANGES = {
        'avg_since_last_modified': {
            'function': get_avg_days_since_mod,
            'range': [(30, 10, None), (31, 45, 8), (46, 60, 5), (61, 90, 2), (91, 0, None)],
            'direction': 1,
            'description': 'Calculating'
        },
        'avg_age': {
            'function': get_avg_age,
            'range': [(45, 10, None), (46, 90, 8), (91, 180, 5), (181, 359, 2), (360, 0, None)],
            'direction': 1,
            'description': 'Calculating age'
        },
        'percent_complete': {
            'function': get_avg_complete,
            'range': [(90, 10, None), (80, 89, 8), (70, 79, 5), (60, 69, 2), (59, 0, None)],
            'description': 'Calculating completion percentage'
        },
        'percent_valid_location': {
            'function': get_percent_valid_location,
            'range': [(90, 10, None), (80, 89, 8), (70, 79, 5), (60, 69, 2), (59, 0, None)],
            'description': 'Analyzing location information'
        },
        'percent_valid_phone': {
            'function': get_percent_valid_phones,
            'range': [(90, 10, None), (80, 89, 8), (70, 79, 5), (60, 69, 2), (59, 0, None)],
            'description': 'Analyzing phone numbers'
        },
        'percent_spam_email': {
            'function': get_percent_spam,
            'range': [(5, 10, None), (5, 7, 8), (7, 9, 5), (9, 12, 2), (12, 0, None)],
            'description': 'Identifying SPAM records'
        },
        'percent_valid_email': {
            'function': get_percent_good_emails,
            'range': [(90, 10, None), (80, 89, 8), (70, 79, 5), (60, 69, 2), (59, 0, None)],
            'description': 'Analyzing email addresses'
        }
    }

    def __init__(self, df, qs_instance=None, location_size=2000, phone_size=2000, email_size=2000, spam_size=2000):
        self.df = df
        self.qs_instance = qs_instance
        self.location_size = location_size
        self.phone_size = phone_size
        self.email_size = email_size
        self.spam_size = spam_size

    def _get_point(self, value, ranges, direction=None):
        if not direction:
            if value >= ranges[0][0]:
                return ranges[0][1]
            elif value <= ranges[-1][0]:
                return ranges[-1][1]
        else:
            if value <= ranges[0][0]:
                return ranges[0][1]
            elif value >= ranges[-1][0]:
                return ranges[-1][1]
        pt = [point for (lower, upper, point) in ranges if lower <= value <= upper]
        return pt[0]

    def calculate(self):
        data = {}
        qscore = 0.0
        for step, range_name in enumerate(self.POINT_RANGES, start=1):
            range_data = self.POINT_RANGES[range_name]
            func = range_data['function']
            point_range = range_data['range']
            direction = range_data.get('direction')
            description = range_data['description']

            point = func(self.df)
            data[range_name] = point
            qscore += self._get_point(point, point_range, direction)

            if self.qs_instance:  # update qs_model progress
                self.qs_instance.progress = '%s' % (float(step * 100) / len(self.POINT_RANGES))
                self.qs_instance.save()

        data['score'] = qscore * 100 / (len(self.POINT_RANGES.keys()) * 10)
        return data
