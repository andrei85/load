from django.test import TestCase

from qscore import prepare
from operations.location_validators import GeocodeValidator
from operations.phone_validators import TwilioValidator
from operations.phone_query import get_is_phone_valid, get_country_code
from quality_score import *
from integrations.salesforce.prepare import test_df


class PercentValidPhoneTestCase(TestCase):

    def setUp(self):
        self.twilio = TwilioValidator()

    def get_percent_valid_location(self):
        # TODO: check result isn't always the same with different datasets
        value = prepare.get_percent_valid_location(test_df, 10)
        self.assertGreaterEqual(value, 0)
        self.assertLowerEqual(value, 100.0)


class PercentValidLocationTestCase(TestCase):

    def setUp(self):
        self.client = GeocodeValidator()

    def test_get_percent_valid_location(self):
        # from integrations.salesforce.prepare import test_df
        from qscore.prepare import get_percent_valid_location
        percent = get_percent_valid_location(test_df, 6)
        self.assertGreaterEqual(percent, 0)


class QualityScoreTestCase(TestCase):

    def test_qscore_range(self):
        qs_calc = QualityScoreCalculator(test_df)
        qscore = qs_calc.calculate()['score']
        self.assertGreaterEqual(qscore, 0)
        self.assertLessEqual(qscore, 100)
