from __future__ import division

import pandas as pd
import numpy as np
from multiprocessing.dummy import Pool as ThreadPool
from pygeolib import GeocoderError

from integrations.salesforce.prepare import remove_extra_columns
from operations.location_validators import GeocodeValidator,ValidationException
from operations.location_query import get_is_location_valid
from operations.email_query import get_is_email_valid, get_is_spam_record
from operations.phone_query import get_is_phone_valid, get_country_code
from utils import LocationBuilder


def get_avg_days_since_mod(df):
    """
    Takes a Pandas dataframe prepared as per prepare.py and returns an
    integer which represents the average number of days since last modified.
    """

    if not type(df) == pd.core.frame.DataFrame:
        raise TypeError    # TODO - what should be done here so that it fails gracefully?
    else:
        df['diff'] = df['now'] - df['Last Modified']

        return df['diff'].mean().days


def get_avg_age(df):
    """
    Takes a Pandas dataframe prepared in prepare.py and returns an integer
    which reprents the average age, in days, of a set of records.
    """

    if not type(df) == pd.core.frame.DataFrame:
        raise TypeError
    else:
        df['age'] = df['now'] - df['Create Date']

        return df['age'].mean().days


def get_random_sample(df, sample_size=100):
    """
    Get n random rows from a DataFrame; return as a new DataFrame, random_df
    """
    if not type(df) == pd.core.frame.DataFrame:
        raise TypeError
        random_rows = False
    else:
        # Get the highest possible location in DataFrame array
        high = len(df.index) - 1
        # Create another DataFrame to store the randomly generated rows from df
        random_df = pd.DataFrame(columns=df.columns)
        # Generate random numbers to pull rows from DataFrame
        random_numbers = np.random.randint(low=0, high=high, size=sample_size)
        count = 0

        for i in random_numbers:
            # Set the a row of random_df to some random row in df
            random_df.loc[count] = df.loc[i]
            count += 1

    return random_df


def get_avg_complete(df):
    """
    Get an average percent of columns
    """
    if not type(df) == pd.core.frame.DataFrame:
        raise TypeError
        completion_percent = False
    else:
        # Set a variable with a Pandas series to avoid destruction to df
        series = df.count()
        # Drop fields that are always filled so as not to skew results
        series = remove_extra_columns(series)
        # Get total possible values if every field in every column was filled
        total_possible_vals = max(series) * len(series)
        total_vals = 0
        # Create a list of the # of filled rows for each column
        for i in series:
            total_vals += i

    # return {
    #     'error': False,
    #     'percent_complete': float(total_vals * 100) / total_possible_vals
    # }
    return total_vals * 100 / total_possible_vals


def get_percent_valid_location(df, size=50):
    """
    calculate the percentage of valid locations in give dataframe
    :param df: dataframe as pandas.core.frame.DataFrame
    :param size: size of dataframe as int
    :return: percentage of valid locations
    """
    df = get_random_sample(df, sample_size=size)
    row_count = len(df)

    index = 0
    locations = []
    while index < row_count:
        locations.append(LocationBuilder({
            'street': str(df.loc[index]['Street']),
            'city': str(df.loc[index]['City']),
            'state': str(df.loc[index]['State/Province']),
            'zipcode': str(df.loc[index]['Zip/Postal Code']),
            'country': str(df.loc[index]['Country'])
        }))
        index += 1

        # try:
        #     validator = location_validator.validate(location)
        # except ValidationException as e:
        #     continue
        #
        # if validator == GeocoderError.G_GEO_OVER_QUERY_LIMIT:
        #     return {
        #         'error': True,
        #         'message': "Can't geocode the address. Geocode query over limit."
        #     }
        # if validator['valid']:
        #     valid_count += 1

    pool = ThreadPool(processes=16)
    results = pool.map(get_is_location_valid, locations)
    valid_count = len([r for r in results if r])

    return valid_count / df.shape[0] * 100


def get_percent_valid_phones(df, sample_size=1000):
    '''
    Take a maximum of `sample_size` samples from a dataframe and calculate
    the percentage of valid phone numbers.

    param df: Pandas dataframe that will be samples.
    param sample_size: Maximum number of samples to take.
    result: Percentage of entries with valid phone numbers.
    '''
    df = get_random_sample(df, sample_size=sample_size)
    args = []
    for index, row in df.iterrows():
        country = row['Country']
        if isinstance(country, basestring):
            country_code = get_country_code(row['Country'])
        else:
            country_code = ''
        args.append((row['Phone'], country_code))


    pool = ThreadPool(processes=16)
    results = pool.map(lambda x: get_is_phone_valid(*x), args)
    valid = len([r for r in results if r['valid']])
    # close the pool and wait for the work to finish
    pool.close()
    pool.join()

    return valid / df.shape[0] * 100


def _get_percent_good_emails(emails):

    pool = ThreadPool(processes=25)  # 25 is limit of Kickbox API: http://docs.kickbox.io/docs/using-the-api#api-limits

    results = pool.map(get_is_email_valid, emails)
    # close the pool and wait for the work to finish
    pool.close()
    pool.join()

    good_email_count = len([result for result in results if result.get('is_valid')])
    return float(good_email_count * 100) / len(emails)


def get_percent_good_emails(df, sample_size=1000):

    df = get_random_sample(df, sample_size=sample_size)

    emails = []
    for index, serie in df.iterrows():
        emails.append(serie['Email'])

    return _get_percent_good_emails(emails)


def get_percent_spam(df, sample_size=1000):

    df = get_random_sample(df, sample_size=sample_size)

    bad_record_count = 0
    good_record_count = 0

    original_count = len(df)
    count = original_count
    while count > 0:
        df_record = df.loc[count - 1]
        is_spam = get_is_spam_record(df_record)
        if is_spam.get('spam'):
            bad_record_count += 1
        else:
            good_record_count += 1

        count -= 1

    return float(bad_record_count * 100) / original_count
