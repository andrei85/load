from operations.email_validators import (
    KickboxChainedValidator,
    SpamContentDictChainedValidator,
    SpamDomainEmailChainedValidator
    )


def get_is_email_valid(email, whitelist=[]):
    message = 'Valid'
    if email in whitelist:
        is_valid = True
    else:
        validator = KickboxChainedValidator(data=email)
        is_valid = validator.is_valid()
        if not is_valid:
            message = validator.errors[0]
    return {
        'is_valid': is_valid,
        'message': message
    }


def get_is_spam_record(df_record):
    """
    record can be dict or pandas.Series instance
    """
    is_spam = False
    spam_score = 0
    message = ''

    spam_email_validator = SpamDomainEmailChainedValidator(df_record['Email'])
    if not spam_email_validator.is_valid():
        is_spam = True
        spam_score += 60
        message += ', '.join(spam_email_validator.errors)

    spam_content_validator = SpamContentDictChainedValidator(df_record)
    if not spam_content_validator.is_valid():
        is_spam = True
        spam_score += 40
        message += ', '.join(spam_content_validator.errors)

    if not message:
         message = 'Valid'

    return {
        'spam': is_spam,
        'spam_score': spam_score,
        'message': message
    }
