import hashlib
import logging
from program_manager import choices
from dateutil.parser import parse as dateutil_parse

class Deduper(object):

    def __init__(self, dataset, dm_rules, sr_rules, sv_rules):
        self.header = dataset[0]
        self.data = dataset[1:]
        self.dm_rules = dm_rules
        self.sr_rules = sr_rules
        self.sv_rules = sv_rules
    
    def preprocess_field(self, value):
        if not value:
            return None
        return value.strip().lower()

    def get_rule_field_index(self, rule):
        # FIXME: use NAMES instead of labels
        return self.header.index(rule.field.get_name())

    def cluster(self):
        match_fields = []
        for dm in self.dm_rules:
            match_fields.append(self.get_rule_field_index(dm))
        hashes = {}
        clusters = {}
        for index, record in enumerate(self.data):
            key = tuple((self.preprocess_field(record[f]) for f in match_fields))
            if None in key:
                continue
            if key in hashes:
                #print 'found duplicate: %s' % str(key)
                hashes[key].append(index)
                clusters[key] = hashes[key]
            else:
                hashes[key] = [index]
        return clusters

    def record_passes_sr_rule(self, cluster, record_index, rule):
        record = cluster[record_index]
        value = record[self.get_rule_field_index(rule)].lower()
        if rule.rule in ['<', '>']:
            try:
                value = float(value)
                target = float(rule.value)
            except ValueError:
                return False
            if rule.rule == '<':
                return value < targetgt
            elif rule.rule == '>':
                return value > target
        elif rule.rule in ['MIN', 'MAX']:
            def sort_key(record):
                return dateutil_parse(record[self.get_rule_field_index(rule)])
            try:
                if rule.rule == 'MIN':
                    other = min(cluster, key=sort_key)
                elif rule.rule == 'MAX':
                    other = max(cluster, key=sort_key)
                if cluster.index(other) == record_index:
                    return True
                else:
                    return False
            except ValueError:
                return False
        else:
            return self.compute_text_rule(rule, value)

    def compute_text_rule(self, rule, value):
        value = value.lower()
        rule.value = rule.value.lower()
        if rule.rule == choices.TEXT_EQUALS:
            return value == rule.value
        elif rule.rule == choices.TEXT_NOT_EQUALS:
            return value != rule.value
        elif rule.rule == choices.TEXT_CONTAINS:
            return rule.value in value
        elif rule.rule == choices.TEXT_STARTS:
            return value.startswith(rule.value)

    def get_canonical_field_value(self, cluster_data, field_id):
        canonical = cluster_data[0][field_id]
        for rule in reversed(self.sv_rules):
            if field_id != self.get_rule_field_index(rule):
                continue
            if rule.rule in ['MIN', 'MAX']:
                if rule.rule == 'MIN':
                    return min(cluster_data, key=self.get_record_last_modified)[field_id]
                elif rule.rule == 'MAX':
                    return max(cluster_data, key=self.get_record_last_modified)[field_id]
            elif rule.rule in ['NUMADD', 'NUMAVG', 'NUMMAX', 'NUMMIN']:
                numbers = []
                for record in cluster_data:
                    try:
                        numbers.append(float(record[field_id]))
                    except ValueError:
                        continue
                if not numbers:
                    return canonical
                if rule.rule == 'NUMADD':
                    return sum(numbers)
                elif rule.rule == 'NUMAVG':
                    return sum(numbers) / float(len(numbers))
                elif rule.rule == 'NUMMAX':
                    return max(numbers)
                elif rule.rule == 'NUMMIN':
                    return min(numbers)
            elif rule.rule == 'CONCAT':
                return '\n\n'.join([f[field_id] for f in cluster_data if f[field_id]])
            else:
                for record in cluster_data:
                    value = record[field_id]
                    if self.compute_text_rule(rule, value):
                        return value
        return canonical

    def sort_cluster(self, cluster_data):
        def sort_key(record):
            return (tuple((self.record_passes_sr_rule(cluster_data,
                cluster_data.index(record), rule) for rule in self.sr_rules)) +
                (self.get_record_last_modified(record),))
        cluster_data = sorted(cluster_data, key=sort_key, reverse=True)
        return cluster_data

    def get_record_last_modified(self, record):
        last_modified = (record[self.header.index('CreatedDate')] or
                         record[self.header.index('LastModifiedDate')])
        return dateutil_parse(last_modified)

    def get_canonical(self, cluster_data):
        master = cluster_data[0]
        canonical = list(master)
        # process sv rules
        for index, field in enumerate(canonical):
            canonical[index] = self.get_canonical_field_value(cluster_data, index)
        # update null fields
        for secondary in cluster_data[1:]:
            for index, value in enumerate(secondary):
                if not canonical[index]:
                    canonical[index] = value
        return canonical

    def get_duplicates(self, extra=None):
        clusters = self.cluster()
        duplicates = []
        for key, cluster in clusters.viewitems():
            cluster_data = self.sort_cluster(map(lambda id: self.data[id], cluster))
            duplicate_record = {'extra': extra,
                                'header': self.header,
                                'canonical': self.get_canonical(cluster_data),
                                'records': cluster_data}
            duplicates.append(duplicate_record)
        return duplicates

