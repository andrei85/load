from pygeolib import GeocoderError

from location_validators import GeocodeValidator, ValidationException

location_validator = GeocodeValidator()


def get_is_location_valid(location):
    """
    Verify if given location is valid or invalid
    :param location: address information as a dictionary
    :return: verify result as a dictionary
    """
    try:
        validator = location_validator.validate(location)
    except ValidationException as e:
        return False

    return validator['valid'] == True
