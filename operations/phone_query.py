import pycountry
from phone_validators import TwilioValidator

validator = TwilioValidator()
def get_is_phone_valid(phone, country='US', extra=False):
    '''
    Verify that a phone is a properly formatted phone number, and return
    extra information, such as the type of the phone number.

    :param phone: Phone number, as a string.
    :return: Dictionary containing the phone number details.
    '''
    return validator.validate(phone, extra=extra, country=country)

country_cache = {}
def get_country_code(country_name):
    '''
    Return the ISO country code from an official or colloquial country name.
    '''
    # TODO: better country parsing with something like geodict or a nlp dataset
    if not country_cache:
        for country in pycountry.countries:
            country_cache[country.alpha2] = country.alpha2
            country_cache[country.name.lower()] = country.alpha2
            if hasattr(country, 'official_name'):
                country_cache[country.official_name.lower()] = country.alpha2
    try:
        country_code = country_cache[country_name.lower()]
    except KeyError:
        country_code = ''
    return country_code
