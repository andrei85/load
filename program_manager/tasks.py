from __future__ import absolute_import

import csv
import rollbar
from celery import shared_task
from celery.utils.log import get_task_logger

from qscore.models import QualityScore
from operations.dedupe import Deduper
from integrations.salesforce.prepare import read_csv
from integrations.salesforce.auth import SalesforceExpiredRefreshToken
from program_manager.models import BatchProgram, Program, ListImportProgram
from qscore.quality_score import QualityScoreCalculator
from integrations.salesforce.utils import translate_program_to_soql, CustomSalesforceBulk

logger = get_task_logger(__name__)

@shared_task
def deduplicate_dataset(dataset, batch_program):
    batch_program = BatchProgram.objects.get(pk=batch_program.pk)
    logger.info('Loading dataset for deduping.')
    dataset = []
    """
    logger.warning('Using test dataset!')
    with open('temp/report1445218513088.csv', 'rb') as data_file:
        for line in csv.reader(data_file):
            dataset.append(line)
    logger.warning('Test data loaded!')
    """
    # FIXME: handle expired or missing token
    # TODO: show any errors to the user
    # TODO: get specific token to this program for multi-token users
    token = batch_program.user.oauthtokens.all()[0]
    token.refresh()
    logger.info('Connecting to the salesforce API.')
    dataset.append([f.name for f in batch_program.table_schema.fields.all()]) # header
    query = translate_program_to_soql(batch_program)
    query += ' LIMIT 30000'
    logger.info('Executing query: %s' % query)
    bulk = CustomSalesforceBulk(sessionId=token.access_token,
                                host=token.instance_url)
    dataset += bulk.process_SOQL_with_auth(query)
    print 'length %s' % (len(dataset) - 1)
    deduper = Deduper(dataset, batch_program.dupmatchrule_set.all(),
                      batch_program.survivingrecordrule_set.all(),
                      batch_program.survivingvaluerule_set.all())
    return deduper.get_duplicates(batch_program.pk)

@shared_task
def apply_dedupe(token, results):
    pass

@shared_task
def analyze_list_import_program(program):
    program = ListImportProgram.objects.get(pk=program.pk)
    program.status = 'RUN'
    program.save()

    df = read_csv(program.csv_file.file)
    try:
        qs = program.get_current_quality_score()
        qs_calc = QualityScoreCalculator(df, qs)
        qscore_data = qs_calc.calculate()
        QualityScore.objects.filter(id=qs.id).update(**qscore_data) # TODO: Is there a more efficient way to do this?
    except Exception as e:
        rollbar.report_exc_info()
        program.status = 'ERROR'
    else:
        program.status = 'COMPL'
    finally:
        program.save()
