import json
import tasks
import choices
from django import forms
from django.contrib import messages
from celery.result import AsyncResult
from django.views.generic import View
from django.utils.html import mark_safe
from django.utils.functional import curry
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.core.exceptions import PermissionDenied
from django.template.response import TemplateResponse
from simple_salesforce import SalesforceExpiredSession
from django.utils.translation import ugettext as _
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import Http404, HttpResponse, JsonResponse
from django.views.generic.base import TemplateResponseMixin
from integrations.salesforce.auth import SalesforceExpiredRefreshToken
from django.views.generic.edit import FormView, DeleteView, UpdateView
from accounts.views import LoginRequiredMixin
from program_manager.forms import (
    FilterForm, DupMatchRuleForm, SurvivingRecordRuleForm,
    SurvivingValueRuleForm, ListImportProgramForm, ProgramSettingsForm,
    ProgramNameForm, DedupeFormSet
    )
from program_manager.models import (
    Program, BatchProgram, DupMatchRule, ListImportProgram, FieldFilter
    )
from qscore.models import QualityScore


class ProgramListView(LoginRequiredMixin, ListView):
    model = Program
    template_name = 'program_manager/list.html'

    def get_context_data(self, **kwargs):
        context = super(ProgramListView, self).get_context_data(**kwargs)
        context['programs'] = Program.objects.filter(user=self.request.user).exclude(status='CREA').exclude(status='ARCH')
        return context


class ProgramDashboardView(ProgramListView):
    template_name = 'program_manager/dashboard.html'


class ProgramSelectType(LoginRequiredMixin, View):
    """
    Create a new program and select its type.
    """
    template_name = 'program_manager/select_program_type.html'

    def get(self, request, pk=None):
        if not pk:
            try:
                token = request.user.oauthtokens.all()[0]
            except IndexError:
                return TemplateResponse(request, 'program_manager/no_oauth.html')
            # FIXME: do this asynchronously
            try:
                token.update_schema()
            except SalesforceExpiredRefreshToken:
                messages.error(request, _('Your Salesforce session has expired, and we\'ve been unable to refresh it. Please add a new Salesforce connection or contact support.'))
                return redirect('programs')
            table_schema = token.salesforce_tables.all()[0]
            program = BatchProgram.objects.create(user=request.user,
                                                  table_schema=table_schema)
            # add default dupmatchrules
            main_email_field = table_schema.get_main_email_field()
            if main_email_field:
                program.dupmatchrule_set.create(priority=0, field=main_email_field)
            return redirect('select_program_type_pk', pk=program.pk)
        program = get_object_or_404(Program, pk=pk, user=request.user)
        context = {'program': program}
        return TemplateResponse(request, self.template_name, context=context)


class ProgramAddFiltersView(LoginRequiredMixin, FormView):
    """
    Allow the user to select a data source, table and add filters.
    """

    template_name = 'program_manager/batch_select.html'
    form_class = FilterForm

    def dispatch(self, request, *args, **kwargs):
        self.program = get_object_or_404(Program, pk=self.kwargs['pk'],
                                                  user=self.request.user)
        return super(ProgramAddFiltersView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        field_filter = form.save(commit=False)
        field_filter.program = self.program
        field_filter.save()
        return super(ProgramAddFiltersView, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(ProgramAddFiltersView, self).get_form_kwargs()
        kwargs['program'] = self.program
        return kwargs

    def get_context_data(self, **kwargs):
        type_table = self.program.table_schema.get_field_types()
        kwargs['type_table'] =  mark_safe(json.dumps(type_table))
        kwargs['program'] = self.program
        kwargs['field_filters'] = self.program.get_field_filters()
        return super(ProgramAddFiltersView, self).get_context_data(**kwargs)

    def get_success_url(self):
        return reverse('batch_select', kwargs=self.kwargs)


class ProgramSelectActionsView(LoginRequiredMixin, View):
    """
    Allow the user to select actions and dedupe rules.
    """
    template_name = 'program_manager/select_actions.html'

    def dispatch(self, request, *args, **kwargs):
        self.program = get_object_or_404(Program, pk=self.kwargs['pk'],
                                                  user=self.request.user)
        operators_requiring_value, _ = zip(*choices.OP_REQUIRES_VALUE)
        self.context = {
            'program': self.program,
            'open_modal': False,
            'operators_requiring_value': json.dumps(operators_requiring_value)
        }
        self.form_classes = {
            'dupmatchrule': DupMatchRuleForm,
            'survivingrecordrule': SurvivingRecordRuleForm,
            'survivingvaluerule': SurvivingValueRuleForm
        }
        self.forms = []
        post_data = None
        if request.method == 'POST':
            post_data = request.POST
        # dedupe rules formsets
        for name, fclass in self.form_classes.viewitems():
            model = fclass.Meta.model
            formset_class = forms.modelformset_factory(model, form=fclass,
                                                       formset=DedupeFormSet,
                                                       extra=0, can_delete=True,
                                                       can_order=True)
            formset_class.form = staticmethod(curry(fclass, program=self.program))
            queryset = model.objects.filter(batch_program=self.program)
            formset =  formset_class(data=post_data, queryset=queryset, prefix=name)
            self.forms.append(formset)
            self.context['%s_formset' % name] = formset
        # program settings
        settings_form = ProgramSettingsForm(data=post_data, instance=self.program,
                                            prefix='programsettings')
        self.forms.append(settings_form)
        self.context['settings_form'] = settings_form
        #self.context['open_modal'] = reduce(lambda a, b: a or b,
        #        map(lambda f: f.errors, reduce(lambda a, b: a or b, self.forms)))
        self.context['open_modal'] = False
        return super(ProgramSelectActionsView, self).dispatch(request, *args, **kwargs)

    def get(self, request, pk=None):
        return TemplateResponse(request, self.template_name, context=self.context)

    def post(self, request, pk=None):
        for form in self.forms:
            if form.is_valid():
                if isinstance(form, forms.BaseFormSet):
                    # handle formsets separately
                    priority = 0
                    instances = form.save(commit=False)
                    subforms = list(form)
                    subforms.sort(key=lambda f: f.cleaned_data['ORDER'])
                    for subform in subforms:
                        model = subform.instance
                        if (not model in form.deleted_objects and
                                not subform.cleaned_data.get('DELETE')):
                            model.priority = priority
                            model.save()
                            priority += 1
                    for model in form.deleted_objects:
                        model.delete()
                else:
                    # save modelforms directly
                    form.save()
                print form.prefix + ' is valid, saved'
        if 'continue' in request.POST:
            return redirect('batch_confirm', pk=self.program.pk)
        else:
            return redirect('select_actions', pk=self.program.pk)


class DeleteFieldFilterView(DeleteView):
    model = FieldFilter

    def get(self, request, *args, **kwargs):
        raise Http404

    def get_queryset(self):
        user = self.request.user
        return super(DeleteFieldFilterView, self).get_queryset().filter(program__user=user)

    def get_success_url(self):
        kwargs = {'pk': self.object.program.pk}
        return reverse('batch_select', kwargs=kwargs)


class ProgramSetNameView(LoginRequiredMixin, UpdateView):
    model = BatchProgram
    form_class = ProgramNameForm
    template_name = 'program_manager/batch_confirm.html'

    def form_valid(self, form):
        program = form.save(commit=False)
        program.status = 'RUN'
        program.save()
        return super(ProgramSetNameView, self).form_valid(form)

    def get_queryset(self):
        user = self.request.user
        return super(ProgramSetNameView, self).get_queryset().filter(user=user)

    def get_success_url(self):
        return reverse('programs')


class CreateListImportProgramView(LoginRequiredMixin, FormView):
    form_class = ListImportProgramForm
    template_name = 'program_manager/import_select.html'

    def form_valid(self, form):
        list_program = form.save(commit=False)
        list_program.user = self.request.user
        list_program.save()
        # run program
        list_program.analyse()
        return redirect(list_program.get_absolute_url())


class ListImportProgramView(LoginRequiredMixin, DetailView):
    template_name = 'program_manager/list_import_program.html'

    def get_queryset(self):
        return ListImportProgram.objects.filter(user=self.request.user)


@login_required
def archive_program(request, pk=None, methods=['GET']):
    program = Program.objects.get(pk=pk, user=request.user)
    if not program:
        return HttpResponse(status=400)

    program.status = 'ARCH'
    program.save()

    return JsonResponse({'status': 'OK'})


@login_required
def qs_data(request, pk):
    if not request.is_ajax():
        return redirect('/')

    qs = get_object_or_404(QualityScore, pk=pk, program__user=request.user)

    return JsonResponse(qs.to_dict())


@login_required
def debug_dedupe(request, pk=None, methods=['GET']):
    '''
    Temporary view to display dedupe results for debug purposes.
    '''

    try:
        program = Program.objects.get(pk=pk, user=request.user)
    except ValueError:
        task = AsyncResult(pk)
        if task.status == 'FAILURE':
            exception = task.get(propagate=False)
            exc_type = exception['exc_type']
            if exc_type == 'SalesforceExpiredRefreshToken':
                return HttpResponse('<h1>Refresh token expired. Please reconnect with Salesforce to continue.</h1>')
            else:
                return HttpResponse('<h1>Unknonw error: %s.</h1>' % exc_type)
        elif not task.ready():
            return HttpResponse('<h1>Dedupe not finished, please refresh.</h1>')
        else:
            #context = {'program': program, 'dupes': dupes[:10]}
            context = {'dupes': task.get(), 'pk': pk}
            return TemplateResponse(request, 'program_manager/debug_dedupe.html', context=context)
    if not program:
        return HttpResponse(status=400)
    dupes = tasks.deduplicate_dataset.delay(None, program)
    return redirect('debug_dedupe', pk=dupes.id)

@login_required
def debug_apply_dedupe(request, pk=None, id=None, methods=['GET']):
    task = AsyncResult(pk)
    results = task.get()
    from integrations.salesforce.soap import SalesforceSOAP
    token = request.user.oauthtokens.all()[0]
    token.refresh()
    soap = SalesforceSOAP(token)
    program = BatchProgram.objects.get(pk=results[0]['extra'])
    updateable = program.table_schema.fields.filter(salesforcefield__updateable=True)
    updateable = [f.name for f in updateable]
    if id == 'all':
        soap.process_dedupe_results(results, updateable)
    else:
        soap.process_dedupe_results([results[int(id)]], updateable)
    return HttpResponse('<h1>Merge finished.</h1>')
