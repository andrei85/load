from django.core.urlresolvers import reverse
from django.db import models
from polymorphic import PolymorphicModel

from accounts.models import CustomUser as User
from integrations.models import ExternalTable, ExternalField
from program_manager.choices import *

class Program(PolymorphicModel):
    """A generic job that contains a connected system (e.g. Salesforce), some
    defined actions and some granular settings."""

    user = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    last_run = models.DateTimeField(null=True, blank=True)
    name = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=256, choices=STATUSES, default='CREA')
    table_schema = models.ForeignKey(ExternalTable, null=True)
    read_only = models.BooleanField(default=False)
    dedupe = models.BooleanField(default=True)
    analyze_only = models.BooleanField(default=False)
    # # Person Info Optimization Options
    # person_name_proper_case = models.BooleanField(default=True)
    # # Company Info Optimization Options
    # address_optimize = models.BooleanField(default=True)
    # phone_optimize = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name or 'unnamed'

    @property
    def progress(self):
        qs = self.get_current_quality_score()
        return qs.progress if qs else None

    def get_current_quality_score(self):
        """Returns the most recently calculated Quality Score"""
        qs = self.get_all_quality_scores()
        return qs[0] if qs else None

    def get_all_quality_scores(self):
        """Returns the most recently calculated Quality Score"""
        return self.qualityscore_set.all().order_by('-id')

    def get_table_filters(self):
        """Returns all TableFilter objects related to Program"""
        return self.tablefilter_set.all()

    def get_field_filters(self):
        """Returns all FieldFilter objects related to Program"""
        return self.fieldfilter_set.all()

    def get_dup_match_rules(self):
        """Returns all DupMatchRule objects related to Program"""
        return self.dupmatchrule_set.all()

    def get_surviving_record_rules(self):
        """Returns all SurvivingRecordRule objects related to Program"""
        return self.survivingrecordrule_set.all()

    def get_surviving_value_rules(self):
        """Returns all SurvivingValueRule objects related to Program"""
        return self.survivingvaluerule_set.all()

    @property
    def is_running(self):
        return self.status == 'RUN'


class Job(models.Model):
    """ Represents a job that is downloading, transforming or updating data as
    part of a Program."""

    program = models.ForeignKey(Program)
    task_id = models.CharField(max_length=128, unique=True)

    def __unicode__(self):
        return self.id


class BatchProgram(Program):
    """A job that runs on some subset of records in a connected system now,
    sometime in the future, or on a recurring schedule."""

    pass


class ListImportProgram(Program):
    csv_file = models.FileField(null=True, default=None)
    # analysis_data = JSONField(default={})

    def analyse(self):
        from program_manager.tasks import analyze_list_import_program
        from qscore.models import QualityScore

        if self.csv_file:
            QualityScore.objects.create(program=self)
            self.status = 'RUN'
            self.save()
            analyze_list_import_program.delay(self)

    def get_absolute_url(self):
        return reverse('import_program_details', kwargs={'pk': self.id})


class TableFilter(models.Model):
    """A table in a connected system.  Tables can be dynamically loaded but
    in initial versions we will hard-code available tables and present these
    table options based on the connected system."""

    batch_program = models.ForeignKey(BatchProgram, null=True, blank=True)
    table_name = models.CharField(max_length=256)  # ex. 'LEAD'


class FieldFilter(models.Model):
    """A field in a connected system and corresponding values that allow the
    user to determine a subset of their records.

    batch_program: the related program
    field: field to filter on from a database table schema
    operator: the appropriate operator based on field type (e.g. >, <, =, older than)
    field_value: a single value or comma-separated values that would make the
        filter true
    """

    program = models.ForeignKey(BatchProgram, null=True, blank=False)
    field = models.ForeignKey(ExternalField)
    operator = models.CharField(max_length=256, default='=', choices=TEXT_OPERATORS+NUM_OPERATORS+DATETIME_OPERATORS+BOOL_OPERATORS)
    field_value = models.CharField(max_length=256, null=True, blank=True)


# class DateFieldFilter(FieldFilter):
#     operator = models.CharField(max_length=256, choices=DATETIME_OPERATORS)
#     value = models.DateField()


class DupMatchRule(models.Model):
    priority = models.IntegerField()
    batch_program = models.ForeignKey(BatchProgram, null=True, blank=True)
    field = models.ForeignKey(ExternalField)
    rule = models.CharField(
        max_length=256,
        choices=MATCH_OPERATORS,
        default='EXACT'
        )

    class Meta:
        ordering = ['priority']


class SurvivingRecordRule(models.Model):
    priority = models.IntegerField()
    batch_program = models.ForeignKey(BatchProgram, null=True, blank=False)
    field = models.ForeignKey(ExternalField)
    rule = models.CharField(
        max_length=256,
        choices=SR_AGE_OPTIONS+SR_NUMBER_OPTIONS+TEXT_OPERATORS,
        default='NEWEST'
        )
    value = models.CharField(max_length=256, null=True, blank=True)

    class Meta:
        ordering = ['priority']


class SurvivingValueRule(models.Model):
    priority = models.IntegerField()
    batch_program = models.ForeignKey(BatchProgram, null=True, blank=False)
    field = models.ForeignKey(ExternalField)
    # field_type = models.CharField(max_length=256)
    rule = models.CharField(
        max_length=256,
        choices=SV_AGE_OPERATORS+SV_NUMBER_OPERATORS+SV_TEXT_OPERATORS+TEXT_OPERATORS,
        default='ADD'
        )
    value = models.CharField(max_length=256, null=True, blank=True)

    class Meta:
        ordering = ['priority']
