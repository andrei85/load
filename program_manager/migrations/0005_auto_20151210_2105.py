# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('program_manager', '0004_job'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dupmatchrule',
            options={'ordering': ['priority']},
        ),
        migrations.AlterModelOptions(
            name='survivingrecordrule',
            options={'ordering': ['priority']},
        ),
        migrations.AlterModelOptions(
            name='survivingvaluerule',
            options={'ordering': ['priority']},
        ),
        migrations.AlterField(
            model_name='survivingvaluerule',
            name='rule',
            field=models.CharField(default=b'ADD', max_length=256, choices=[(b'MIN', b'keep the oldest value'), (b'MAX', b'keep the newest value'), (b'NUMADD', b'add all values'), (b'NUMAVG', b'take an average of all values'), (b'NUMMAX', b'keep the highest value'), (b'NUMMIN', b'keep the lowest value'), (b'IS', b'is'), (b'NOT', b'is not'), (b'CONTAINS', b'contains'), (b'STARTS', b'starts with')]),
        ),
    ]
