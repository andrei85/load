# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('program_manager', '0002_auto_20151209_1545'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survivingrecordrule',
            name='rule',
            field=models.CharField(default=b'NEWEST', max_length=256, choices=[(b'MIN', b'is oldest value'), (b'MAX', b'is newest value'), (b'<', b'is the lowest value'), (b'>', b'is the highest value'), (b'IS', b'is'), (b'NOT', b'is not'), (b'CONTAINS', b'contains'), (b'STARTS', b'starts with')]),
        ),
    ]
