# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('program_manager', '0005_auto_20151210_2105'),
    ]

    operations = [
        migrations.AddField(
            model_name='job',
            name='task_id',
            field=models.CharField(default='', unique=True, max_length=128),
            preserve_default=False,
        ),
    ]
