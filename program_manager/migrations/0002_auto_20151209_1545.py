# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('program_manager', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survivingvaluerule',
            name='rule',
            field=models.CharField(default=b'ADD', max_length=256, choices=[(b'MIN', b'keep the oldest value'), (b'MAX', b'keep the newest value'), (b'ADD', b'add all values'), (b'AVG', b'take an average of all values'), (b'MAX', b'keep the highest value'), (b'MIN', b'keep the lowest value')]),
        ),
    ]
