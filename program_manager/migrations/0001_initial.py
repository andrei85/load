# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('integrations', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DupMatchRule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('priority', models.IntegerField()),
                ('rule', models.CharField(default=b'EXACT', max_length=256, choices=[(b'EXACT', b'is an exact match'), (b'SIMILAR', b'is very similar'), (b'LOOSE', b'is somewhat similar')])),
                ('field', models.ForeignKey(to='integrations.ExternalField')),
            ],
        ),
        migrations.CreateModel(
            name='FieldFilter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('operator', models.CharField(default=b'=', max_length=256, choices=[(b'IS', b'is'), (b'NOT', b'is not'), (b'CONTAINS', b'contains'), (b'STARTS', b'starts with'), (b'=', b'equals'), (b'>', b'is greater than'), (b'<', b'is less than'), (b'DATE_EXACT', b'is'), (b'DATE_AFTER', b'is after'), (b'DATE_BEFORE', b'is before'), (b'TRUE', b'True'), (b'FALSE', b'False')])),
                ('field_value', models.CharField(max_length=256, null=True, blank=True)),
                ('field', models.ForeignKey(to='integrations.ExternalField')),
            ],
        ),
        migrations.CreateModel(
            name='Program',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_run', models.DateTimeField(null=True, blank=True)),
                ('name', models.CharField(max_length=256, null=True, blank=True)),
                ('status', models.CharField(default=b'CREA', max_length=256, choices=[(b'CREA', b'Creating'), (b'PEND', b'Pending'), (b'RUN', b'Running'), (b'PAUSE', b'Paused'), (b'ERROR', b'Error'), (b'COMPL', b'Completed'), (b'ACT', b'Active'), (b'ARCH', b'Archived')])),
                ('read_only', models.BooleanField(default=False)),
                ('dedupe', models.BooleanField(default=True)),
                ('analyze_only', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SurvivingRecordRule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('priority', models.IntegerField()),
                ('rule', models.CharField(default=b'NEWEST', max_length=256, choices=[(b'MIN', b'is oldest value'), (b'MAX', b'is newest value'), (b'MIN', b'is the lowest value'), (b'MAX', b'is the highest value'), (b'IS', b'is'), (b'NOT', b'is not'), (b'CONTAINS', b'contains'), (b'STARTS', b'starts with')])),
                ('value', models.CharField(max_length=256, null=True, blank=True)),
                ('field', models.ForeignKey(to='integrations.ExternalField')),
            ],
        ),
        migrations.CreateModel(
            name='SurvivingValueRule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('priority', models.IntegerField()),
                ('rule', models.CharField(default=b'ADD', max_length=256, choices=[(b'MIN', b'keep the oldest value'), (b'MAX', b'keep the newest value'), (b'ADD', b'add all values'), (b'AVG', b'take an average of all values'), (b'MAX', b'keep the highest value'), (b'MIN', b'keep the lowest value'), (b'IS', b'is'), (b'NOT', b'is not'), (b'CONTAINS', b'contains'), (b'STARTS', b'starts with')])),
                ('value', models.CharField(max_length=256, null=True, blank=True)),
                ('field', models.ForeignKey(to='integrations.ExternalField')),
            ],
        ),
        migrations.CreateModel(
            name='TableFilter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('table_name', models.CharField(max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='BatchProgram',
            fields=[
                ('program_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='program_manager.Program')),
            ],
            options={
                'abstract': False,
            },
            bases=('program_manager.program',),
        ),
        migrations.CreateModel(
            name='ListImportProgram',
            fields=[
                ('program_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='program_manager.Program')),
                ('csv_file', models.FileField(default=None, null=True, upload_to=b'')),
            ],
            options={
                'abstract': False,
            },
            bases=('program_manager.program',),
        ),
        migrations.AddField(
            model_name='program',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='polymorphic_program_manager.program_set+', editable=False, to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='program',
            name='table_schema',
            field=models.ForeignKey(to='integrations.ExternalTable', null=True),
        ),
        migrations.AddField(
            model_name='program',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='tablefilter',
            name='batch_program',
            field=models.ForeignKey(blank=True, to='program_manager.BatchProgram', null=True),
        ),
        migrations.AddField(
            model_name='survivingvaluerule',
            name='batch_program',
            field=models.ForeignKey(to='program_manager.BatchProgram', null=True),
        ),
        migrations.AddField(
            model_name='survivingrecordrule',
            name='batch_program',
            field=models.ForeignKey(to='program_manager.BatchProgram', null=True),
        ),
        migrations.AddField(
            model_name='fieldfilter',
            name='program',
            field=models.ForeignKey(to='program_manager.BatchProgram', null=True),
        ),
        migrations.AddField(
            model_name='dupmatchrule',
            name='batch_program',
            field=models.ForeignKey(blank=True, to='program_manager.BatchProgram', null=True),
        ),
    ]
