from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

from program_manager import views

urlpatterns = [
    url(r'^$', views.ProgramDashboardView.as_view(), name='programs'),
    url(r'^list/$', views.ProgramListView.as_view(), name='program_list'),
    url(r'^select_program_type/$', views.ProgramSelectType.as_view(), name='select_program_type'),
    url(r'^select_program_type/(?P<pk>[0-9]+)$',views.ProgramSelectType.as_view(), name='select_program_type_pk'),
    url(r'^analysis_select/$',login_required(TemplateView.as_view(template_name='program_manager/analysis_select.html')),name='analysis_select'),
    url(r'^analysis_confirm/$',login_required(TemplateView.as_view(template_name='program_manager/analysis_confirm.html')),name='analysis_confirm'),
    url(r'^batch_select/(?P<pk>[0-9]+)$',views.ProgramAddFiltersView.as_view(),name='batch_select'),
    url(r'^select_actions/(?P<pk>[0-9]+)$',views.ProgramSelectActionsView.as_view(),name='select_actions'),
    url(r'^batch_confirm/(?P<pk>[0-9]+)$',views.ProgramSetNameView.as_view(),name='batch_confirm'),
    url(r'^fieldfilter/(?P<pk>[0-9]+)/delete',views.DeleteFieldFilterView.as_view(),name='delete_fieldfilter'),
    url(r'^import_select/$',views.CreateListImportProgramView.as_view(),name='import_select'),
    url(r'^import_program/(?P<pk>[0-9]+)/$', views.ListImportProgramView.as_view(), name='import_program_details'),
    url(r'^(?P<pk>[0-9]+)/archive$', views.archive_program, name='archive_program'),
    url(r'^qs_data/(?P<pk>[0-9]+)/$', views.qs_data, name='qs_data'),
    url(r'^debug_dedupe/(?P<pk>[-\w]+)$', views.debug_dedupe, name='debug_dedupe'),
    url(r'^debug_apply_dedupe/(?P<pk>[-\w]+)/(?P<id>[-\w]+)$', views.debug_apply_dedupe, name='debug_apply_dedupe'),
]
