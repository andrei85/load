from django.contrib import admin

from program_manager.models import ListImportProgram
from .models import (
    Program, BatchProgram, TableFilter, FieldFilter, SurvivingRecordRule,
    SurvivingValueRule, DupMatchRule
    )
from qscore.models import QualityScore

class QualityScoreInline(admin.TabularInline):
    model = QualityScore
    extra = 0


class TableFilterInline(admin.TabularInline):
    model = TableFilter
    extra = 0


class FieldFilterInline(admin.TabularInline):
    model = FieldFilter
    extra = 0


class DupeMatchRuleInline(admin.TabularInline):
    model = DupMatchRule
    extra = 0


class SurvivingRecordRuleInline(admin.TabularInline):
    model = SurvivingRecordRule
    extra = 0


class SurvivingValueRuleInline(admin.TabularInline):
    model = SurvivingValueRule
    extra = 0


class DupMatchRuleInline(admin.TabularInline):
    model = DupMatchRule
    extra = 0


class BatchProgramAdmin(admin.ModelAdmin):
    model = BatchProgram
    inlines = [
        TableFilterInline,
        FieldFilterInline,
        SurvivingRecordRuleInline,
        SurvivingValueRuleInline,
        QualityScoreInline,
        DupMatchRuleInline
    ]
    # readonly_fields = ('status',)
    list_filter = ('status',)


admin.site.register(BatchProgram, BatchProgramAdmin)
admin.site.register(ListImportProgram)
