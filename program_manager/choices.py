STATUSES = (
    ('CREA', 'Creating'),
    ('PEND', 'Pending'),
    ('RUN', 'Running'),
    ('PAUSE', 'Paused'),    # When a RealTime Program is inactive, use this
    ('ERROR', 'Error'),
    ('COMPL', 'Completed'),
    ('ACT', 'Active'),   # Only RealTimeProgram can have this status.
    ('ARCH', 'Archived')   # Only RealTimeProgram can have this status.
)
TEXT_EQUALS = 'IS'
TEXT_NOT_EQUALS = 'NOT'
TEXT_CONTAINS = 'CONTAINS'
TEXT_STARTS = 'STARTS'

TEXT_OPERATORS = (
    (TEXT_EQUALS, 'is'),
    (TEXT_NOT_EQUALS, 'is not'),
    (TEXT_CONTAINS, 'contains'),
    (TEXT_STARTS, 'starts with'),
)
NUM_EQUALS = '='
NUM_GREATER = '>'
NUM_LESS = '<'
NUM_OPERATORS = (
    (NUM_EQUALS, 'equals'),
    (NUM_GREATER, 'is greater than'),
    (NUM_LESS, 'is less than'),
)
DATETIME_EQUALS = 'DATE_EXACT'
DATETIME_AFTER = 'DATE_AFTER'
DATETIME_BEFORE = 'DATE_BEFORE'
DATETIME_OPERATORS = (
    (DATETIME_EQUALS, 'is'),
    (DATETIME_AFTER, 'is after'),
    (DATETIME_BEFORE, 'is before'),
)
BOOL_TRUE = 'TRUE'
BOOL_FALSE = 'FALSE'
BOOL_OPERATORS = (
    (BOOL_TRUE, 'True'),
    (BOOL_FALSE, 'False'),
)
MATCH_OPERATORS = (
    ('EXACT', 'is an exact match'),
    ('SIMILAR', 'is very similar'),
    ('LOOSE', 'is somewhat similar'),
)

# Below are choices for SurvivingRecordRule
SR_AGE_OPTIONS = (
    ('MIN', 'is oldest value'),
    ('MAX', 'is newest value'),
)
SR_NUMBER_OPTIONS = (
    ('<', 'is the lowest value'),
    ('>', 'is the highest value'),
)

# Below are choices for SurvivingValueRule; broken up into separate variables
# so that in the future, we can supply choices based on field types
SV_AGE_OPERATORS = (
    ('MIN', 'keep the oldest value'),
    ('MAX', 'keep the newest value'),
)
SV_NUMBER_OPERATORS = (
    ('NUMADD', 'add all values'),
    ('NUMAVG', 'take an average of all values'),
    ('NUMMAX', 'keep the highest value'),
    ('NUMMIN', 'keep the lowest value'),
)

SV_TEXT_OPERATORS = (
    ('CONCAT', 'concatenate all text'),
)

# List of dedupe operators that require values
OP_REQUIRES_VALUE = TEXT_OPERATORS
