from django import forms
from django.utils.html import format_html
from django.forms import ModelChoiceField
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe
from dateutil.parser import parse as dateutil_parse
from django.utils.translation import ugettext_lazy as _

from program_manager import choices
from program_manager.models import (
        BatchProgram, FieldFilter, DupMatchRule, SurvivingRecordRule,
        SurvivingValueRule, ListImportProgram
        )


class BatchProgramNameForm(forms.Form):
    class Meta():
        model = BatchProgram
        fields = ('name')


operator_map = {
        'string': choices.TEXT_OPERATORS + choices.MATCH_OPERATORS,
        'boolean': choices.BOOL_OPERATORS,
        'int': choices.NUM_OPERATORS,
        'double': choices.NUM_OPERATORS,
        'date': choices.DATETIME_OPERATORS,
        'datetime': choices.DATETIME_OPERATORS,
        'binary': choices.TEXT_OPERATORS,
        'id': choices.NUM_OPERATORS,
        'reference': choices.NUM_OPERATORS,
        'currency': choices.NUM_OPERATORS,
        'percent': choices.NUM_OPERATORS,
        'phone': choices.TEXT_OPERATORS,
        'url': choices.TEXT_OPERATORS + choices.MATCH_OPERATORS,
        'email': choices.TEXT_OPERATORS + choices.MATCH_OPERATORS,
        'picklist': choices.TEXT_OPERATORS + choices.MATCH_OPERATORS,
        'multipicklist': choices.TEXT_OPERATORS + choices.MATCH_OPERATORS,
        'location': choices.TEXT_OPERATORS + choices.MATCH_OPERATORS,
        'anyType': choices.TEXT_OPERATORS + choices.NUM_OPERATORS + choices.DATETIME_OPERATORS + choices.BOOL_OPERATORS + choices.MATCH_OPERATORS
}


class OperatorSelect(forms.Select):

    def render_option(self, selected_choices, option_value, option_label):
        if option_value is None:
            option_value = ''
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        # generate list of css classes from the operator map
        css_classes = []
        for field_type, operators in operator_map.viewitems():
            for op, _ in operators:
                if op == option_value:
                    css_classes.append('opfilter_' + field_type)
        return format_html('<option value="{}" class="{}" {}>{}</option>',
                           option_value,
                           ' '.join(css_classes),
                           selected_html,
                           force_text(option_label))

class FilterForm(forms.ModelForm):

    def __init__(self, program, *args, **kwargs):
        super(FilterForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.viewitems():
            field.widget.attrs['class'] = 'form-control'
        queryset = program.table_schema.fields.all()
        self.fields['field'].queryset = queryset
        # remove empty option
        self.fields['field'].empty_label = None
        self.fields['field'].widget.choices = self.fields['field'].choices

    def clean(self):
        cleaned_data = super(FilterForm, self).clean()
        field_value = cleaned_data.get('field_value')
        bool_ops, bool_ops_names = zip(*choices.BOOL_OPERATORS)
        num_ops, num_ops_names = zip(*choices.NUM_OPERATORS)
        date_ops, date_ops_names = zip(*choices.DATETIME_OPERATORS)
        op = cleaned_data.get('operator')
        if not field_value and not op in bool_ops:
            self.add_error('field_value', forms.ValidationError(_('This field is required.'), code='required'))
        if field_value:
            if op in num_ops:
                try:
                    float(field_value)
                except ValueError:
                    self.add_error('field_value', forms.ValidationError(_('This operator requires a numeric value.'), code='invalid'))
            elif op in date_ops:
                try:
                     dateutil_parse(field_value)
                except ValueError:
                    self.add_error('field_value', forms.ValidationError(_('This operator requires a datetime value.'), code='invalid'))
        return cleaned_data

    class Meta():
        model = FieldFilter
        fields = ['field', 'operator', 'field_value']
        widgets = {
            'operator': OperatorSelect
        }

class DedupeFormSet(forms.BaseModelFormSet):
    pass

class DedupeRuleForm(forms.ModelForm):

    def __init__(self, program, *args, **kwargs):
        super(DedupeRuleForm, self).__init__(*args, **kwargs)
        self.program = program
        for key, field in self.fields.viewitems():
            current_class = field.widget.attrs.get('class', '')
            new_class = (current_class + ' form-control form-wide').strip()
            if key == 'field':
                new_class += ' selectpicker'
                field.widget.attrs['data-live-search'] = 'true'
            field.widget.attrs['class'] = new_class
        queryset = self.program.table_schema.fields.all()
        self.fields['field'].queryset = queryset
        # remove empty option
        self.fields['field'].empty_label = None
        self.fields['field'].widget.choices = self.fields['field'].choices

    def get_set(self):
        return self.Meta.model.objects.filter(batch_program=self.program)

    def save(self, commit=True):
        instance = super(DedupeRuleForm, self).save(commit=False)
        instance.batch_program = self.program
        #instance.priority = self.get_set().count()
        if commit:
            instance.save()
        return instance

class DupMatchRuleForm(DedupeRuleForm):

    class Meta():
        model = DupMatchRule
        fields = ['field', 'rule']
        labels = {'field': '', 'rule': ''}

class SurvivingRecordRuleForm(DedupeRuleForm):

    class Meta():
        model = SurvivingRecordRule
        fields = ['field', 'rule', 'value']
        labels = {'field': '', 'rule': '', 'value': ''}
        widgets = {
            'rule': forms.Select(attrs={'class': 'rule-operator'}),
            'value': forms.TextInput(attrs={'class': 'rule-value'})
        }

class SurvivingValueRuleForm(DedupeRuleForm):

    class Meta():
        model = SurvivingValueRule
        fields = ['field', 'rule', 'value']
        labels = {'field': '', 'rule': '', 'value': ''}
        widgets = {
            'rule': forms.Select(attrs={'class': 'rule-operator'}),
            'value': forms.TextInput(attrs={'class': 'rule-value'})
        }

class ProgramSettingsForm(forms.ModelForm):

    class Meta():
        model = BatchProgram
        fields = ['dedupe', 'analyze_only']
        labels = {'dedupe': 'Dedupe', 'analyze_only': ''}


class ProgramNameForm(forms.ModelForm):

    class Meta():
        model = BatchProgram
        fields = ['name']
        labels = {'name': ''}


class ListImportProgramForm(forms.ModelForm):
    class Meta:
        model = ListImportProgram
        fields = ('name', 'csv_file')
