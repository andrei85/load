# from django.db.models.signals import post_save
# from django.dispatch import receiver
#
# from program_manager.models import Program
# from qscore.quality_score import get_quality_score
# from qscore.models import QualityScore
#
# # TODO - this should be a queued task via Celery/RabbitmQ
# @receiver(post_save, sender=Program)
# def create_quality_score(sender, instance, **kwargs):
#     if instance.status in ('PEND', 'COMPL'):
#         if instance.csv_file:
#             score = int(get_quality_score(df))
#             qscore_obj = QualityScore(score=score, program=instance)
#             qscore_obj.save()
#             instance.status = 'COMPL'
#             instance.save()
#         else:
#             pass    # TODO make this generic for all programs!
