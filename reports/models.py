from django.db import models

from accounts.models import CustomUser as User
from program_manager.models import Program, BatchProgram


class Report(models.Model):
    user = models.ForeignKey(User)
    batch_program = models.OneToOneField(BatchProgram, null=True, blank=True)
    # realtime_program
    # listimport_program

    def __unicode__(self):
        if self.batch_program.name:
            return self.batch_program.name
        else:
            pass


class QualityScoreReport(Report):
    """A reporting object that focuses on Quality Score as it relates to a
    set of data and Program."""

    pass

    def get_current_quality_score(self):
        """Returns the most recently calculated Quality Score object"""
        return self.batch_program.qualityscore_set.all().order_by('-id')[0]

    def get_all_quality_scores(self):
        return self.batch_program.qualityscore_set.all().order_by('id')


class LogsReport(Report):
    """ Detailed logging output related to a program """

    pass
