# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('program_manager', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='LogsReport',
            fields=[
                ('report_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='reports.Report')),
            ],
            bases=('reports.report',),
        ),
        migrations.CreateModel(
            name='QualityScoreReport',
            fields=[
                ('report_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='reports.Report')),
            ],
            bases=('reports.report',),
        ),
        migrations.AddField(
            model_name='report',
            name='batch_program',
            field=models.OneToOneField(null=True, blank=True, to='program_manager.BatchProgram'),
        ),
        migrations.AddField(
            model_name='report',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
