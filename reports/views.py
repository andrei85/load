from django.views.generic.list import ListView
from django.views.generic import DetailView
from accounts.views import LoginRequiredMixin

# from reports.models import QualityScoreReport
from program_manager.models import Program


class ReportListView(LoginRequiredMixin, ListView):
    # model = QualityScoreReport
    template_name = 'reports/list.html'

    def get_queryset(self, **kwargs):
        return QualityScoreReport.objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(ReportListView, self).get_context_data(**kwargs)
        context['reports'] = QualityScoreReport.objects.filter(user=self.request.user)

        return context


class QualityScoreOverTimeView(LoginRequiredMixin, DetailView):
    model = Program
    template_name = 'reports/qot.html'

    def get_context_data(self, **kwargs):
        context = super(QualityScoreOverTimeView, self).get_context_data(**kwargs)

        return context


class QualityScoreSnapshot(LoginRequiredMixin, DetailView):
    model = Program
    template_name = 'reports/snapshot.html'

    def get_context_data(self, **kwargs):
        context = super(QualityScoreSnapshot, self).get_context_data(**kwargs)

        return context
