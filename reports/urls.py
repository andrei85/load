from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

from reports.views import (
    ReportListView,
    QualityScoreOverTimeView,
    QualityScoreSnapshot
    )

urlpatterns = [
    url(r'^$', ReportListView.as_view(), name='reports'),
    url(r'^qot/(?P<pk>[-\w]+)/$', QualityScoreOverTimeView.as_view(), name='report_qot'),
    url(r'^snapshot/(?P<pk>[-\w]+)/$', QualityScoreSnapshot.as_view(), name='report_snapshot'),

#     url(r'^select_program_type/$', login_required(
#         TemplateView.as_view(template_name='program_manager/select_program_type.html')), name='select_program_type'),
#     url(r'^analysis_select/$', login_required(
#         TemplateView.as_view(template_name='program_manager/analysis_select.html')), name='analysis_select'),
#     url(r'^analysis_confirm/$', login_required(
#         TemplateView.as_view(template_name='program_manager/analysis_confirm.html')), name='analysis_confirm'),
#     url(r'^batch_select/$', login_required(
#         TemplateView.as_view(template_name='program_manager/batch_select.html')), name='batch_select'),
#     url(r'^select_actions/$', login_required(
#         TemplateView.as_view(template_name='program_manager/select_actions.html')), name='select_actions'),
#     url(r'^batch_confirm/$', login_required(
#         TemplateView.as_view(template_name='program_manager/batch_confirm.html')), name='batch_confirm'),
    ]
