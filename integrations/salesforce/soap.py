import requests
from sforce.partner import SforcePartnerClient

class SalesforceSOAP(object):

    def __init__(self, token):
        self.token = token
        profile = requests.post(token.id_url, data={'access_token': self.token.access_token})
        self.partner_url = profile.json()['urls']['partner'].replace('{version}', '35.0')
        self.client = SforcePartnerClient('integrations/salesforce/partner.wsdl')
        self.force_oauth_login(self.token.access_token, self.partner_url)
        self.merge_batches = []
        self.merge_queue = []

    def force_oauth_login(self, access_token, soap_endpoint):
        self.client._setHeaders('login')
        header = self.client.generateHeader('SessionHeader')
        header.sessionId = access_token
        self.client.setSessionHeader(header)
        self.client._sessionId = access_token
        self.client._setEndpoint(soap_endpoint)

    def append_merge(self, merge_request):
        self.merge_queue.append(merge_request)
        if len(self.merge_queue) > 6:
            self.merge_batches.append(self.merge_queue)
            self.merge_queue = []

    def commit_merges(self):
        self.merge_batches.append(self.merge_queue)
        self.merge_queue = []
        for batch in self.merge_batches:
            result = self.client.merge(batch)
            print result

    def merge_group(self, group, header_index, canonical, header, updateable):
        if len(group) == 1:
            return group[0]
        elif len(group) <= 3:
            master = self.client.generateObject('Lead')
            master.Id = group[0][header_index]
            if master.Id == canonical[header_index]:
                for i in range(0, len(header)):
                    if canonical[i]:
                        if  header[i] in updateable:
                            setattr(master, header[i], canonical[i])
            merge_request = self.client.generateObject('MergeRequest')
            merge_request.masterRecord = master
            merge_request.recordToMergeIds = []
            #merge_request.recordToMergeIds = ''
            for i in range(1, len(group)):
                merge_request.recordToMergeIds.append(group[i][header_index])
                #merge_request.recordToMergeIds += ' ' + group[i][header_index]
                #merge_request.recordToMergeIds = group[i][header_index]
            #print 'request: %s' % str(merge_request)
            self.append_merge(merge_request)
            return group[0]
        new_group = []
        for i in range(0, len(group), 3):
            new_group.append(self.merge_group(group[i:i + 3], header_index, canonical, header, updateable))
        return self.merge_group(new_group, header_index, canonical, header, updateable)

    def process_dedupe_results(self, results, updateable, table='Lead'):
        for result in results:
            header_index = result['header'].index('Id')
            self.merge_group(result['records'], header_index, result['canonical'], result['header'], updateable)
        self.commit_merges()
