from django.db import models
from django.contrib import admin
from django.contrib import messages
from django.forms import TextInput, Textarea
from django.utils.translation import ugettext as _
from simple_salesforce import SalesforceExpiredSession

from integrations.salesforce.models import OauthToken, SalesforceTable, SalesforceField, PicklistValue

class SalesforceFieldInlineAdmin(admin.TabularInline):
    model = SalesforceField
    # TODO: limit number of fields
    formfield_overrides = {
            models.CharField: {'widget': TextInput(attrs={'size': '10'})},
            models.TextField: {'widget': TextInput(attrs={'size': 10})}
    }

class OauthTokenAdmin(admin.ModelAdmin):
    actions = ['update_schema']

    def update_schema(self, request, queryset):
        for token in queryset:
            try:
                token.update_schema()
            except SalesforceExpiredSession:
                self.message_user(request, 'Unable to update schema for token %s: the session token has expired, and couldn\'t be refreshed.' % token, messages.ERROR)

    update_schema.short_description = "Force Salesforce schema update"

class SalesforceTableAdmin(admin.ModelAdmin):
    inlines = [SalesforceFieldInlineAdmin]

admin.site.register(OauthToken, OauthTokenAdmin)
admin.site.register(SalesforceTable, SalesforceTableAdmin)
