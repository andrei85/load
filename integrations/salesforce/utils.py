import os
from salesforce_bulk import SalesforceBulk
from django.http import HttpResponse
from django.http import HttpResponse
from salesforce_bulk import SalesforceBulk
from boto.s3.key import Key
from django.conf import settings
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.core.cache import cache
from program_manager.choices import *
from program_manager.utils import django_model_instance_to_dict
import urlparse
import cStringIO as StringIO
import csv
import urlparse
import boto


class BulkApiError(Exception):

    def __init__(self, message, status_code=None):
        super(BulkApiError, self).__init__(message)
        self.status_code = status_code

class CustomSalesforceBulk(SalesforceBulk):

    def __init__(self, sessionId=None, host=None, username=None, password=None, token=None,
                 client_id=None, client_secret=None, exception_class=BulkApiError, API_version="29.0"):
        if not sessionId and not username:
            raise RuntimeError("Must supply either sessionId/instance_url or username/password")
        if not sessionId:
            sessionId, endpoint = CustomSalesforceBulk.login_to_salesforce(username, password, token, client_id, client_secret)
            host = urlparse.urlparse(endpoint)
            host = host.hostname.replace("-api", "")

        if host[0:4] == 'http':
            self.endpoint = host
        else:
            self.endpoint = "https://" + host
        self.endpoint += "/services/async/%s" % API_version
        self.sessionId = sessionId
        self.jobNS = 'http://www.force.com/2009/06/asyncapi/dataload'
        self.jobs = {}  # dict of job_id => job_id
        self.batches = {}  # dict of batch_id => job_id
        self.batch_statuses = {}
        self.exception_class = exception_class


    def process_SOQL_with_auth(self, query, table="Lead"):
        """ Takes a SOQL query and a specified table and returns a DictReader
        object. """

        print "Process SOQL with Salesforce authentication"
        job = self.create_query_job(table, contentType='CSV')
        batch = self.query(job, query)
        # self.wait_for_batch(job, batch)
        # self.close_job(job_id)
        dataArray = []

        file = default_storage.open('before', 'w')

        result_ids = self.get_batch_result_ids(batch_id=batch, job_id=job)
        for result_id in result_ids:
            for row in self.get_batch_results(job_id=job, result_id=result_id, batch_id=batch, parse_csv=True):
                dataArray.append(row)
                file.write(row)

        file.close()
        default_storage.delete('before')
        return dataArray

    def write_csv(self, data, filename):
        bucket_name = settings.AWS_STORAGE_BUCKET_NAME
        conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY)
        bucket = conn.get_bucket(bucket_name)
        bucket_list = bucket.list()
        for l in bucket_list:
            keyString = str(l.key)


    def csv(request):
        csvfile = StringIO.StringIO()
        csvwriter = csv.writer(csvfile)

        def read_and_flush():
            csvfile.seek(0)
            data = csvfile.read()
            csvfile.seek(0)
            csvfile.truncate()
            return data

        def data():
            for i in xrange(10):
                csvwriter.writerow([i,"a","b","c"])
            data = read_and_flush()
            yield data

        response = HttpResponse(data(), mimetype="text/csv")
        response["Content-Disposition"] = "attachment; filename=test.csv"
        return response

    def export_csv(self, data):
        """ Takes a DictReader object as created by process_SOQL_with_auth and
        returns a CSV file """

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="data.csv"'

        writer = csv.writer(response)
        writer.writerows(data)



    @staticmethod
    def login_to_salesforce(username, password, token, client_id, client_secret):
        try:
            import salesforce_oauth_request
            from salesforce_oauth_request.utils import load_user_info
        except ImportError:
            raise ImportError(
                "You must install salesforce-oauth-request to use username/password")
        data = {'access_token': token,
                'id': 'https://ca--FSB2.cs19.my.salesforce.com'}
        user_info = load_user_info(data)
        print user_info
        # packet = salesforce_oauth_request.login(username=username, password=password, token=token, client_id=client_id, client_secret=client_secret)
        # return packet['access_token'], packet['instance_url']
        return data['access_token'], data['id']


class SalesforceQueryParser(object):
    OPERATOR_TYPE_TEXT = 'TEXT'
    OPERATOR_TYPE_NUM = 'NUM'
    OPERATOR_TYPE_DATETIME = 'DATETIME'
    OPERATOR_TYPE_BOOL = 'BOOL'
    OPERATOR_TYPE_MATCH = 'MATCH'

    OPERATOR_TYPES = {
        OPERATOR_TYPE_TEXT: TEXT_OPERATORS,
        OPERATOR_TYPE_NUM: NUM_OPERATORS,
        OPERATOR_TYPE_DATETIME: DATETIME_OPERATORS,
        OPERATOR_TYPE_BOOL: BOOL_OPERATORS,
        OPERATOR_TYPE_MATCH: MATCH_OPERATORS
    }

    OPERATORS_MAP = {
        TEXT_EQUALS: ('=', lambda val: "'%s'" % val),
        TEXT_NOT_EQUALS: ('!=', lambda val: "'%s'" % val),
        TEXT_CONTAINS: ('LIKE', lambda val: "'%%%s%%'" % val),
        TEXT_STARTS: ('LIKE', lambda val: "'%s%%'" % val),
        NUM_EQUALS: ('=', lambda val: '%s' % val),
        NUM_GREATER: ('>', lambda val: '%s' % val),
        NUM_LESS: ('<', lambda val: '%s' % val),
        DATETIME_EQUALS: ('=', lambda val:  '%s' % val),
        DATETIME_AFTER: ('>', lambda val:  '%s' % val),
        DATETIME_BEFORE: ('<', lambda val: '%s' % val),
        BOOL_TRUE: ('=', lambda val:  'TRUE'),
        BOOL_FALSE: ('=', lambda val: 'FALSE'),
    }

    def __init__(self, schema, fields, fieldfilter_set):
        self.schema = schema
        self.fields = fields
        self.fieldfilter_set = fieldfilter_set

    def get_operator_type(self, operator, default=OPERATOR_TYPE_TEXT):
        for operator_type, operators in self.OPERATOR_TYPES.iteritems():
            if operator in dict(operators).keys():
                return operator_type
        return default

    def make_where_clause(self, operator, field_name, field_value):
        field_value = str(field_value)
        if ',' in field_value:
            field_value = '(%s)' % ' OR '.join(
                [self.OPERATORS_MAP[operator][1](v.strip()) for v in field_value.split(',')])
        else:
            field_value = self.OPERATORS_MAP[operator][1](field_value)

        operator = self.OPERATORS_MAP[operator][0]

        return '%s %s %s' % (field_name, operator, field_value)

    def fieldfilter_set_to_query(self):
        """
        :return: Saleforce Query as strings
        """
        wheres = []
        for fieldfilter in self.fieldfilter_set:
            operator = fieldfilter.get('operator')
            field_value = fieldfilter.get('field_value')
            field_data = fieldfilter.get('field')
            field_name = field_data.get('name')
            wheres.append(self.make_where_clause(operator, field_name, field_value))
        if wheres:
            return 'SELECT %s FROM %s WHERE %s' % (','.join(self.fields), self.schema, ' AND '.join(wheres))
        else:
            return 'SELECT %s FROM %s' % (','.join(self.fields), self.schema)

def translate_program_to_soql(program):
    """ Build complete SOQL statement to retrieve all possible fields and
     applying all related FieldFilters."""

    # Translate entire program + foreignkeys to dict
    pdict = django_model_instance_to_dict(program, depth=2)
    # Get the table that the Program is tied to
    table = program.table_schema.name
    # Get all fields in the table
    all_fields = []
    for i in program.table_schema.fields.all():
        all_fields.append(i.name)

    parser = SalesforceQueryParser(table, all_fields, pdict['fieldfilter_set'])
    return parser.fieldfilter_set_to_query()

