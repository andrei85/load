from django.db import models
from django.conf import settings
from polymorphic import PolymorphicManager
from django.utils.translation import ugettext_lazy as _
from simple_salesforce import Salesforce, SalesforceExpiredSession, SalesforceGeneralError

from integrations.salesforce.auth import get_prod_auth, refresh_token
from integrations.models import ArrayField, ExternalTable, ExternalField


class OauthToken(models.Model):
    """
    Stores access tokens for Salesforce CRM
    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        db_index=True,
        related_name='oauthtokens',
        )
    salesforce_user_id = models.CharField(
        verbose_name=_('Salesforce User ID'),
        max_length=256,
        null=True,
        blank=True,
        )
    salesforce_organization_id = models.CharField(
        verbose_name=_('Salesforce Org Id'),
        max_length=256,
        null=True,
        blank=True,
        )
    password = models.CharField(    # TODO: This needs to be encrypted!!
        verbose_name=_('Salesforce password'),
        max_length=256,
        null=True,
        blank=True
        )
    instance_url = models.URLField(
        verbose_name=_('Instance URL'),
        null=True,
        blank=True,
        )
    access_token = models.CharField(
        verbose_name=_('Access Token'),
        max_length=256,
        null=True,
        blank=True,
        )
    id_url = models.CharField(
        verbose_name=_('ID URL'),
        max_length=256,
        null=True,
        blank=True,
        )
    id_token = models.CharField(  # TODO: This needs to be encrypted!!
        verbose_name=_('ID Token'),
        max_length=256,
        null=True,
        blank=True,
        )
    refresh_token = models.CharField(  # TODO: This needs to be encrypted!!
        verbose_name=_('Refresh Token'),
        max_length=256,
        null=True,
        blank=True,
        )
    issued_at = models.DateTimeField(
        verbose_name=_('Issued At'),
        null=True,
        blank=True,
        )
    active = models.BooleanField(
        verbose_name=_('Active'),
        default=False,
        db_index=True,
        )
    thumbnail_photo = models.URLField(
        verbose_name=_('Thumbnail Photo'),
        null=True,
        blank=True,
        )
    is_sandbox = models.BooleanField(
        default=False
        )
    # timezone = models.CharField(
    #     max_length=256,
    #     verbose_name=_('Timezone'),
    #     null=True,
    #     blank=True,
    #     )

    def __unicode__(self):
        return str(self.user.email)

    def save(self, *args, **kwargs):
        update_schema = kwargs.pop('update_schema', False)
        super(OauthToken, self).save(*args, **kwargs)
        if update_schema:
            self.update_schema()

    def refresh(self, force=False):
        """
        Refreshes this token if the session has expired.

        :param force: Refresh this token even if the session hasn't expired
        """
        has_expired = False
        try:
            connection = Salesforce(instance_url=self.instance_url,
                                    session_id=self.access_token,
                                    sandbox=self.is_sandbox)
            connection.describe()
        except (SalesforceExpiredSession, SalesforceGeneralError):
            has_expired = True
        if has_expired or force:
            refresh_token(self)
            return Salesforce(instance_url=self.instance_url,
                                    session_id=self.access_token,
                                    sandbox=self.is_sandbox)
            return connection

    def update_schema(self):
        self.refresh()
        auth = get_prod_auth(self)
        # TODO: load all the tables
        for table in self.salesforce_tables.filter(name='Lead'):
            self.salesforce_tables.remove(table)
        lead_schema = auth.Lead.describe()
        lead_table = self.salesforce_tables.create(name='Lead')
        for field in lead_schema['fields']:
#            lead_table.fields.create_from_description(field)
             picklist_values = field['picklistValues']
             field['table'] = lead_table
             del field['picklistValues']
             field = SalesforceField.objects.create(**field)
             for picklist in picklist_values:
                 field.picklistValues.create(**picklist)

    class Meta:
        verbose_name = _('Oauth token')
        verbose_name_plural = _('Oauth tokens')


class SalesforceTable(ExternalTable):
    """Describes a table in a Salesforce database."""

    oauthtoken = models.ForeignKey(OauthToken, related_name='salesforce_tables', null=True, blank=True)

#class SalesforceFieldManager(PolymorphicManager):
#
#    user_for_related_fields = True
#
#    def create_from_description(self, description):
#        """
#        Create a Salesforce field from a description returned from their API.
#        Warning: This call modifies the description.
#        """
#        picklist_values = description['picklistValues']
#        del description['picklistValues']
#        field = self.create(**description)
#        for picklist in picklist_values:
#            field.picklistValues.create(**picklist)
#        return field
#


class SalesforceField(ExternalField):
    """
    Describes a field in a Salesforce table.
    """

    autoNumber = models.BooleanField()
    byteLength = models.PositiveIntegerField()
    calculated = models.BooleanField()
    calculatedFormula = models.TextField(null=True, blank=True)
    cascadeDelete = models.BooleanField()
    caseSensitive = models.BooleanField()
    controllerName = models.CharField(max_length=4096, null=True, blank=True)
    createable = models.BooleanField()
    custom = models.BooleanField()
    defaultValue = models.TextField(null=True, blank=True)
    defaultValueFormula = models.TextField(null=True, blank=True)
    defaultedOnCreate = models.BooleanField()
    dependentPicklist = models.BooleanField()
    deprecatedAndHidden = models.BooleanField()
    digits = models.PositiveIntegerField()
    displayLocationInDecimal = models.BooleanField()
    externalId = models.BooleanField()
    filterable = models.BooleanField()
    groupable = models.BooleanField()
    htmlFormatted = models.BooleanField()
    idLookup = models.BooleanField()
    inlineHelpText = models.TextField(null=True, blank=True)
    label = models.CharField(max_length=1024)
    length = models.PositiveIntegerField()
    name = models.CharField(max_length=1024)
    nameField = models.BooleanField()
    namePointing = models.BooleanField()
    nillable = models.BooleanField()
    permissionable = models.BooleanField()
    #picklistValues - reverse foreign key (PicklistValue)
    precision = models.PositiveSmallIntegerField()
    referenceTo = ArrayField()
    relationshipName = models.CharField(max_length=1024, null=True, blank=True)
    relationshipOrder = models.PositiveSmallIntegerField(null=True, blank=True)
    restrictedDelete = models.BooleanField()
    restrictedPicklist = models.BooleanField()
    scale = models.PositiveIntegerField()
    soapType = models.CharField(max_length=1024)
    sortable = models.BooleanField()
    type = models.CharField(max_length=1024)
    unique = models.BooleanField()
    updateable = models.BooleanField()
    writeRequiresMasterRead = models.BooleanField()

    #objects = SalesforceFieldManager()

    def get_label(self):
        return self.label

    def get_name(self):
        return self.name

    def get_type(self):
        mappings = {
            'base64': 'binary',
            'textarea': 'string',
            'combobox': 'string'}
        return mappings.get(self.type, self.type)

    def __unicode__(self):
        return u'%s' % self.label

class PicklistValue(models.Model):
    """An item in a Salesforce picklist."""
    schema_field = models.ForeignKey(SalesforceField, related_name='picklistValues')
    active = models.BooleanField()
    defaultValue = models.BooleanField()
    label = models.CharField(max_length=512)
    validFor = models.CharField(max_length=64, null=True, blank=True) # TODO: check how the Salesforce API retrives this field (bitfield)
    value = models.TextField()
