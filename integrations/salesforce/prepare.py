"""
Prepares some data pulled from Salesforce prior to passing to qscore.calculate
or other functions that rely on columns and row values being formatted in a
specific way.
"""

import datetime

import pandas as pd
from pandas import Timestamp
from salesforce_bulk import CsvDictsAdapter

from rl_proto2.settings import BASE_DIR
from auth import get_dev_auth_for_bulk


def read_csv(csv_file):
    csv_df = pd.read_csv(csv_file)

    # Format Last Modified in acceptable Pandas format
    csv_df['Last Modified'] = pd.to_datetime(csv_df['Last Modified'])
    csv_df['Create Date'] = pd.to_datetime(csv_df['Create Date'])

    # Add a new column and set every row to current date & time
    now = datetime.datetime.now()
    csv_df['now'] = Timestamp(now)
    return csv_df


def get_test_data():
    temp = BASE_DIR + '/temp/report_short.csv'
    return read_csv(temp)


def remove_extra_columns(series):
    """
    Takes a Pandas Series and removes system-generated fields and our own
    fields.  Prerequisite for measurement and ansalysis type functions.
    """
    trimmed = series.drop(
        ['Create Date', 'Last Modified', 'Lead Owner', 'now', 'age'],
        errors='ignore'
        )

    return trimmed

def bulk_import_csv(data, count=0, repeat=1):

    bulk = get_dev_auth_for_bulk()
    job = bulk.create_insert_job("Lead", contentType='CSV')

    row_count = len(data)
    if count != 0:
        row_count = min(len(data), count)

    for i in range(repeat):
        leads = [dict(FirstName=data.loc[idx]["First Name"],
                      LastName=data.loc[idx]["Last Name"],
                      Company=data.loc[idx]["Company Name"],
                      Title=data.loc[idx]["Job Title"],
                      Email=data.loc[idx]["Email"],
                      Phone=data.loc[idx]["Phone"],
                      Street=data.loc[idx]["Address"],
                      City=data.loc[idx]["City"],
                      State=data.loc[idx]["State"],
                      PostalCode=data.loc[idx]["Postal Code"],
                      Country=data.loc[idx]["Country"],
                      Website=data.loc[idx]["Company Website"]) for idx in xrange(row_count)]

        csv_iter = CsvDictsAdapter(iter(leads))
        batch = bulk.post_bulk_batch(job, csv_iter)
        bulk.wait_for_batch(job, batch)

    bulk.close_job(job)
    print str(row_count*repeat) + " Leads are pushed successfully."


# set test_data variable for use in various tests
test_df = get_test_data()
