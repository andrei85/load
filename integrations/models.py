import json

from django.db import models
from polymorphic import PolymorphicModel, PolymorphicManager
from django.core.serializers.json import DjangoJSONEncoder

# TODO: use django.contrib.postgres.fields.ArrayField for PostreSQL deployment
class ArrayField(models.TextField):
    """
    Stores a list of strings.
    """

    __metaclass__ = models.SubfieldBase

    def to_python(self, value):
        if value == '':
            return None
        elif isinstance(value, basestring):
            #return json.loads(value)
            return value
        return value

    def get_db_prep_save(self, value, *args, **kwargs):
        if value == '':
            return None
        if isinstance(value, list):
            value = json.dumps(value, cls=DjangoJSONEncoder)
        return super(ArrayField, self).get_db_prep_save(value, *args, **kwargs)

class ExternalTable(PolymorphicModel):
    """
    A generic database schema, as provided from any of the available data sources.
    """

    name = models.CharField(max_length=512)

    objects = PolymorphicManager()

    def get_field_types(self):
        '''
        Returns a dict matching field labels to field types.
        '''
        types = {}
        for field in self.fields.all():
            types[field.label] = field.type
        return types

    def get_main_email_field(self):
        '''
        Returns the email field most likely to represent the main contact address
        for the entry, or None if there are no candidates.
        '''
        first_email = None
        for field in self.fields.all():
            if field.get_label().lower() == 'email':
                return field
            if field.get_type() == 'email' and not first_email:
                first_email = field
        return first_email

    def __unicode__(self):
        return u'%s' % self.name


class ExternalField(PolymorphicModel):
    """
    The base class that contains the definition of a database field, to be
    inherited by integrations providing data sources.
    """

    table = models.ForeignKey(ExternalTable, related_name='fields')

    objects = PolymorphicManager()

    def get_label(self):
        """
        Returns the human-friendly name of this field.
        """
        raise NotImplementedError

    def get_name(self):
        """
        Returns the identifier of this field.
        """
        raise NotImplementedError

    def get_type(self):
        """
        Returns the type of this field. May be one of:
          * string
          * boolean
          * int
          * double
          * date
          * datetime
          * binary
          * id
          * reference
          * currency
          * percent
          * phone
          * url
          * email
          * picklist
          * multipicklist
          * anyType
          * location
        """
        raise NotImplementedError

